## random_fortune

## Description
This is a fortune randmizer that selects at random a fortune catagory to display in a terminal.  it is designed to be used as part of the login sequence (bash ~/.profile, etc.), but can be called as a standard command.


## Installation
Copy this script to a location in your home directory that is in your $PATH. (eg: ~/bin) and configure your login session to call this script as part of its setup routine.  Make sure to read the script and update the variables to match your system as needed.

## Usage
There are no command options used.  install into a location included in $PATH and run it.


## License
For open source projects, say how it is licensed.

