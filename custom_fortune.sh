#!/usr/bin/bash



# randomly pick a fortune database to supply to fortune command

#main fortune database
FortuneDatabaseMain="/usr/share/fortune"

#supplemental fortunes (including your own)  using array format:
#eg ("/path/to/your/custom/fortunes")
FortuneDatabaseExtras=()

Exclude="art ascii-art"

options=()

function exists_in_list() {
    LIST=$1
    DELIMITER=$2
    VALUE=$3
    echo "$LIST" | tr "$DELIMITER" '\n' | grep -F -q -x "$VALUE"
}

candidates=$(find "$FortuneDatabaseMain" | cut -d '/' -f 5 | cut -d "." -f 1)

for candidate in $candidates
do
 #echo "$candidate"
 if exists_in_list "$Exclude" " " "$candidate"; then
    continue
 fi

 options+=("$candidate")
 
done

for custompath in "${FortuneDatabaseExtras[@]}"
do
 #shellcheck disable=SC2012
 optionalcandidates=$(ls -1 $"$custompath" | cut -d '.' -f 1 | uniq)
 
 for optionalcandidate in $optionalcandidates
 do
  if exists_in_list "$Exclude" " " "$optionalcandidate"; then
   continue
  fi
  
  options+=("$optionalcandidate")


 done


done


size=${#options[@]}
randomindex=$((RANDOM%"$size"))

#echo "$randomindex"
#echo ${options[$randomindex]}
fortune "${options[$randomindex]}"
